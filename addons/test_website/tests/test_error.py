import juniper.tests
from juniper.tools import mute_logger


@juniper.tests.common.tagged('post_install', '-at_install')
class TestWebsiteError(juniper.tests.HttpCase):

    @mute_logger('juniper.addons.http_routing.models.ir_http', 'juniper.http')
    def test_01_run_test(self):
        self.start_tour("/test_error_view", 'test_error_website')
