# -*- coding: utf-8 -*-
# Part of Juniper. See LICENSE file for full copyright and licensing details.

import juniper.tests
from juniper.tools import mute_logger


@juniper.tests.common.tagged('post_install', '-at_install')
class TestCustomSnippet(juniper.tests.HttpCase):

    @mute_logger('juniper.addons.http_routing.models.ir_http', 'juniper.http')
    def test_01_run_tour(self):
        self.start_tour("/", 'test_custom_snippet', login="admin")
