# -*- coding: utf-8 -*-
# Part of Juniper. See LICENSE file for full copyright and licensing details.

from unittest.mock import patch

import juniper
from juniper.tests import tagged
from juniper.tests.common import HttpCase


@tagged('post_install', '-at_install')
class TestWebsiteSaleMail(HttpCase):

    def test_01_shop_mail_tour(self):
        """The goal of this test is to make sure sending SO by email works."""

        self.env['product.product'].create({
            'name': 'Acoustic Bloc Screens',
            'list_price': 2950.0,
            'website_published': True,
        })
        self.env['res.partner'].create({
            'name': 'Azure Interior',
            'email': 'azure.Interior24@example.com',
        })

        # we override unlink because we don't want the email to be auto deleted
        MailMail = juniper.addons.mail.models.mail_mail.MailMail

        main_website = self.env.ref('website.default_website')
        other_websites = self.env['website'].search([]) - main_website

        # We change the domain of the website to test that the email that
        # will be sent uses the correct domain for its links.
        main_website.domain = "my-test-domain.com"
        other_websites.domain = "https://domain-not-used.fr"
        with patch.object(MailMail, 'unlink', lambda self: None):
            self.start_tour("/", 'shop_mail', login="admin")
