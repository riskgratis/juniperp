# -*- coding: utf-8 -*-
# Part of Juniper. See LICENSE file for full copyright and licensing details.

from juniper import fields, models


class Company(models.Model):
    _inherit = 'res.company'

    l10n_in_upi_id = fields.Char(string="UPI Id")
