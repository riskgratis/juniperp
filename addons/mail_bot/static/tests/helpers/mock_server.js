/** @juniper-module **/

import MockServer from 'web.MockServer';

MockServer.include({
    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    /**
     * @override
     */
    async _performRpc(route, args) {
        if (args.model === 'mail.channel' && args.method === 'init_juniperbot') {
            return this._mockMailChannelInitJuniperBot();
        }
        return this._super(...arguments);
    },

    //--------------------------------------------------------------------------
    // Private Mocked Methods
    //--------------------------------------------------------------------------

    /**
     * Simulates `init_juniperbot` on `mail.channel`.
     *
     * @private
     */
    _mockMailChannelInitJuniperBot() {
        // TODO implement this mock task-2300480
        // and improve test "JuniperBot initialized after 2 minutes"
    },
});
