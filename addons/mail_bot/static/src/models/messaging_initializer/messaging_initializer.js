/** @juniper-module **/

import { registerInstancePatchModel } from '@mail/model/model_core';

registerInstancePatchModel('mail.messaging_initializer', 'mail_bot/static/src/models/messaging_initializer/messaging_initializer.js', {
    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    /**
     * @private
     */
    async _initializeJuniperBot() {
        const data = await this.async(() => this.env.services.rpc({
            model: 'mail.channel',
            method: 'init_juniperbot',
        }));
        if (!data) {
            return;
        }
        this.env.session.juniperbot_initialized = true;
    },

    /**
     * @override
     */
    async start() {
        await this.async(() => this._super());

        if ('juniperbot_initialized' in this.env.session && !this.env.session.juniperbot_initialized) {
            this._initializeJuniperBot();
        }
    },
});
