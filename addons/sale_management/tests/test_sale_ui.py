# Part of Juniper. See LICENSE file for full copyright and licensing details.

from juniper.addons.account.tests.common import AccountTestInvoicingCommon
from juniper.tests.common import tagged, HttpCase


@tagged('post_install', '-at_install')
class TestUi(AccountTestInvoicingCommon, HttpCase):

    def test_01_sale_tour(self):
        self.start_tour("/web", 'sale_tour', login="admin", step_delay=100)
