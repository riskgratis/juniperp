# -*- coding: utf-8 -*-
# Part of Juniper. See LICENSE file for full copyright and licensing details.

import juniper.tests

@juniper.tests.tagged("post_install", "-at_install")
class TestJuniperEditor(juniper.tests.HttpCase):

    def test_juniper_editor_suite(self):
        self.browser_js('/web_editor/static/lib/juniper-editor/test/editor-test.html', "", "", timeout=1800)
